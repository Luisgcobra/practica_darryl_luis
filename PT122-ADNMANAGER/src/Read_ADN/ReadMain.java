/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Read_ADN;

import java.util.Scanner;

/**
 *
 * @author oscar
 */
public class ReadMain {

    private String[] menuOptions
            = {"Sortir",
                "Donar la volta a l’ADN. (retorn String)",
                "Trobar la base més repetida. (retorn caràcter)",
                "Trobar la base menys repetida. (retorn caràcter)",
                "Convertir l’ADN a ARN (2*)",
                "Número de vegades que apareix un tros de cadena ADN. (3*)"
            };

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ReadMain myApp = new ReadMain();
        AppAdn lectura = new AppAdn();
        lectura.leerfichero();
        myApp.run();
    }

    private void run() {

        //  loadDAta();
        int option = -1;
        do {
            option = showMenu();
            switch (option) {
                case 1:
                    System.out.println("Donar la volta a l’ADN. (retorn String)");

                    break;
                case 2:
                    System.out.println("Trobar la base més repetida. (retorn caràcter)");

                    break;
                case 3:
                    System.out.println("Trobar la base menys repetida. (retorn caràcter)");

                    break;
                case 4:
                    System.out.println("Convertir l’ADN a ARN (2*)");

                    break;
                case 5:
                    System.out.println("Número de vegades que apareix un tros de cadena ADN. (3*)");

                    break;

            }

        } while (option != 0);
        System.out.println("You have choosen the option " + option);
    }

    private int showMenu() {
        int option = -1;
        System.out.println("Menu");
        for (int i = 0; i < menuOptions.length; i++) {
            // System.out.println(i + ")"+ menuOptions[i]);
            //  String texto = String.format("%d)%s", i, menuOptions[i]);
            // System.out.println(texto);
            System.out.println(String.format(
                    "%d)%s", i, menuOptions[i]));
        }
        System.out.print("Choose your option:");
        Scanner lector = new Scanner(System.in);
        try {
            option = lector.nextInt();
        } catch (Exception e) {
            System.out.println("Option not valid");
        }

        return option;

    }
}