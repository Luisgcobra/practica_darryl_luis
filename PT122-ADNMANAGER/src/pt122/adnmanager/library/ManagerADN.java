package pt122.adnmanager.library;

/**
 * Llibreria que conté mètodes per a tractar cadenes d'ADN.
 * @author mique
 */
public class ManagerADN {
    
    int ContA;
    int ContT;
    int ContG;
    int ContC;
    
    public ManagerADN() {
        ContA=0; 
        ContT=0;
        ContG=0;
        ContC=0;
    }
    //- Donar la volta a l’ADN. (retorn String)
    /**
     * 
     * @param ADN
     * @return True si es correcte, False si no es un ADN valid.
     */
    public boolean EsCorrecteADN(String ADN) {
        // Un ADN valid nomes pot tenir els caracters A,C,G,T.
        char[] caractersADN = ADN.toCharArray();
        boolean result = true;
        
        for (char c : caractersADN) {
            switch (c) {
                case 'A':
                    ContA++;
                    break;
                case 'T':
                    ContT++;
                    break;
                case 'G':
                    ContG++;
                    break;
                case 'C':
                    ContC++;
                    break;
                default:
                    result = false;
                    break;
            }
        }

        return result;
    }
    
    /**
     * Retorna la cadena ADN passada per paràmtre invertida.
     * @param ADN Cadena ADN.
     * @return Cadena ADN Invertida
     */
    public static String donarVoltaADN(String ADN) {
        return "";
    }
    
    
//- Trobar la base més repetida. (retorn caràcter)
    public static String baseMesRepetida(String ADN) {
        // Un ADN valid nomes pot tenir els caracters A,C,G,T.
        return "";
    }
    
//- Trobar la base menys repetida. (retorn caràcter)
    
    public static String baseMenysRepetida(String ADN) {
        // Un ADN valid nomes pot tenir els caracters A,C,G,T.
        return "";
    }
    
    //- Fer recompte de bases i mostrar-lo. (1*)ç
    public static String recompteBases(String ADN) {
        // Un ADN valid nomes pot tenir els caracters A,C,G,T.
        return "";
    }
    
//- Convertir l’ADN a ARN (2*)
    public static String ADNtoARN(String ADN) {
        // Un ADN valid nomes pot tenir els caracters A,C,G,T.
        return "";
    }
    
//- Número de vegades que apareix un tros de cadena ADN. (3*)
    
//    https://gitlab.com/dawbio2-m15-uf1-a01-2020/provant-exp-regulars/-/blob/master/ProvaExpRegulars/src/provaexpregulars/ExpRegularsADN.java
//    // Contar cuantas veces aparece en un ADN la cadena "cat" o "gat"
//       ADNcadena = "catagacatcat";
//       System.out.println("cat cuantas veces aparece en el ADN catagacatcat? "); 
//       pat = Pattern.compile("cat");
//       mat = pat.matcher(ADNcadena);
//       int cont = 0;
//       while (mat.find()) {
//           cont++;
//       }

    
//- Sortir.

}
