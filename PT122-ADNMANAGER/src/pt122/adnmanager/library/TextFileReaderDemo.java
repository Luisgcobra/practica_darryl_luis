/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt122.adnmanager.library;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;

/**
 *
 * @author mique
 */
public class TextFileReaderDemo {
    
    public static void main(String[] args) {
        
        // ruta absoluta; es menos elegante pero vale.
        // String ruta 
        //  = "C:\\Users\\mique\\Documents\\NetBeansProjects\\introprogoo\\PlantillaM15Pt22ControlVersionsADN";
        
        //exemple de LECTURA d'un arxiu a Java
        try {

            boolean trobat = false;

            // Ruta relativa
            // https://stackoverrun.com/es/q/580537
            
            File fileURL = new File("users.txt");
            //vull obrir un arxiu de texte d'usuaris en mode lectura.
            FileInputStream fstream = new FileInputStream(fileURL);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;

            // I vull trobar un usuari i contrassenya concrets.
            String usuari = "admin";
            String contrasenya = "admin";

            while ((strLine = br.readLine()) != null) {

               String n = strLine.split(":")[0];
               String c = strLine.split(":")[1];
               System.out.println("Llegint usuari " + n);

               if (n.equals(usuari) && c.equals(contrasenya)) {
                   trobat=true; //he trobat a l'usuari
                 break;
              }
              
            }
            System.out.println("Hem trobat l'usuari i contrassenya que voliem ? " 
                    + (trobat?"Sí":"No"));
            
            in.close();

            }catch (Exception e){
               System.out.println(e.getMessage());
            }
            //...instruccions
    }    
}
/*}*/
